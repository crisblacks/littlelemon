import React, { useState } from "react";
import { Alert, Image, StyleSheet, Text, TextInput, View } from "react-native";
import Button from "../components/Button";
import { validateEmail } from "../utils";

const SubscribeScreen = () => {
  const [emailAddress, setEmailAddress] = useState('');

  const emailIsValid = validateEmail(emailAddress);

  return (
    <View style={styles.mainWrapper}>
      <Image
        style={styles.icon}
        source={require("../assets/little-lemon-icon-grey.png")}
      />
      <Text style={styles.headerText}>
        Sign up for our newsletter!
      </Text>
      <TextInput
        style={styles.emailInput}
        keyboardType="email-address"
        textContentType="emailAddress"
        value={emailAddress}
        onChangeText={setEmailAddress}
        placeholder="Enter email"
      />
      <Button
        handlePress={() => {
          Alert.alert("Thank you for subscribing!");
        }}
        isDisabled={!emailIsValid}

        label="Sign Up"
      >
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  mainWrapper: {
    flex: 1,
    padding: 24,
    backgroundColor: "white",
  },
  headerText: {
    color: "#333333",
    textAlign: "center",
    fontSize: 20,
  },
  icon: {
    height: 100,
    width: 300,
    resizeMode: "contain",
    marginBottom: 32,
  },
  emailInput: {
    height: 40,
    marginVertical: 24,
    borderRadius: 8,
    borderWidth: 1,
    padding: 10,
    fontSize: 16,
    borderColor: "#EDEFEE",
  },
});

export default SubscribeScreen;
