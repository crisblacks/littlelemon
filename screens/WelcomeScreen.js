import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import Button from "../components/Button";

const WelcomeScreen = ({ navigation }) => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.innerContainer}>
        <Image
          style={styles.appLogo}
          source={require("../assets/little-lemon-icon.png")}
        />
        <Text style={styles.header}>
          Little Lemon, the best resturant!
        </Text>
      </View>
      <Button
        handlePress={() => {
          navigation.navigate("Subscribe");
        }}

        label="Join our Newsletter"
      >
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    padding: 20,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appLogo: {
    height: 200,
    width: 300,
    resizeMode: "contain",
  },
  header: {
    marginTop: 50,
    paddingVertical: 10,
    color: "#333333",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold",
  },
});

export default WelcomeScreen;
