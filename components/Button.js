import React from "react";
import { Pressable, StyleSheet, Text } from "react-native";

const Button = ({ handlePress, label, isDisabled }) => {
  return (
    <Pressable
      onPress={handlePress}
      style={[styles.buttonContainer, isDisabled && styles.buttonDisabled]}
      disabled={isDisabled}
    >
      <Text style={styles.buttonText}>{label}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    borderRadius: 7,
    backgroundColor: '#495A55',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 7,
  },
  buttonDisabled: {
    backgroundColor: 'grey',
    opacity: 0.5,
  },
  buttonText: {
    fontSize: 17,
    color: 'white',
  },
});

export default Button;