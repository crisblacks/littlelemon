export const validateEmail = (email) => {
  return email.match(
    /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/
  );
};
